# Evoluum Cache
Componente focado em fazer cache lógico de dados.

## Problema a ser resolvido
É muito comum um softare fazer request a serviços externos que fogem do nosso controle.
Assim quando esse sistema estiver offline, não deveríamos ficar offline.

## Solução
Este componente tem com objetivo criar um cache dos dados com expiração lógica.

Cenários:

```
  1 - O sistema valida se o cache existe:
    1.1 - Se não existir, ele executa normalmente e gera o cache novo.
    1.2 - Se já existe o cache, ele verifica se o cache está expirado:
      1.2.1 - Se não estiver expirado, retorna os dados do cache.
      1.2.2 - Se tiver expirado, ele chama o método normal:
        1.2.2.1 - Se o método normal funcionar ele atualizao cache e retorna o valor.
        1.2.2.2 - Se der algum erro no acesso externo, ele retorna o dado do cache, mesmo expirado.
```

## Como utililzar
1. Adicionar dependência

```
<dependency>
    <groupId>com.br.evoluum.cache</groupId>
    <artifactId>evoluum-cache</artifactId>
    <version>1.0.0</version>
</dependency>
```

2. Habilitando cache
No Application adicionar a anotação

```
@EnabledEvoluumCache
```

3. Anotação do cache
No método que desejar fazer cache, usar a anotação abaixo.

```
@EvoluumCacheable(name = "customCpf", expires = CacheExpires.THIRTY_SECONDS)
```

4. Exception
O proceso de cache tem 5 exceptions importantes que correspondem ao erro de acesso ao serviço externo.
Então é muito importante que o @Service que for chamar o serviço externo, quando der algum erro, lance uma dessas exceptions,
porque assim o componente de Cache executará o Circuit Break.

```
BadGatewayException
ExternalErrorException
GatewayTimeoutException
InternalServerErrorException
ServiceUnavailableException
```