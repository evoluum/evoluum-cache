package com.br.evoluum.cache;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CacheExpires {
    THIRTY_SECONDS(30),
    ONE_MINUTE(60),
    ONE_HOUR(3600),
    ONE_DAY(86400),
    SEVEN_DAYS(604800);

    private int second;
}
