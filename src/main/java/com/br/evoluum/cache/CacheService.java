package com.br.evoluum.cache;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    @Autowired
    private RedisTemplate<String, Object> template;

    @Value("${spring.redis.cache.enabled:true}")
    private boolean cacheEnabled;

    public void clearAllCaches() {
        template.execute((RedisCallback<Object>) connection -> {
            connection.flushAll();
            return null;
        });
    }

    private void cachePut(String key, Object toBeCached, long ttlMinutes) {
        if (!cacheEnabled)
            return;

        template.opsForValue().set(key, toBeCached);
    }

    public void cachePut(String key, Object toBeCached) {
        if (!cacheEnabled)
            return;

        if (toBeCached == null)
            return;

        cachePut(key, toBeCached, -1);
    }

    public Object cacheGet(String key) {
        if (!cacheEnabled)
            return null;

        return template.opsForValue().get(key);

    }

}