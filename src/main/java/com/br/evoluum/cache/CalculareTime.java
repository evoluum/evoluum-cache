package com.br.evoluum.cache;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class CalculareTime {

    public String[] sum(CacheExpires cacheExpires, Date date){

        final DateFormat dt = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
        final Calendar c = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        c.setTime(date);

        String dateOriginal = dt.format(date);

        c.add(Calendar.SECOND, cacheExpires.getSecond());

        return new String[] {dateOriginal, dt.format(c.getTime())};
    }
}
