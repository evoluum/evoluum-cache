package com.br.evoluum.cache;

public class CheckedException {

    public static boolean check(Exception e) {
        return
                e.getClass().getName().toLowerCase().indexOf("timeout") >= 0 ||
                        e.getClass().getName().toLowerCase().indexOf("serviceunavailable") >= 0 ||
                        e.getClass().getName().toLowerCase().indexOf("externalerror") >= 0;

    }

}
