package com.br.evoluum.cache;

import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AutoConfigurationPackage
@Import({CacheService.class, EvoluumCacheIntercept.class,
        ExpiratedTime.class, RedisCacheConfig.class, CalculareTime.class,
        GenerateKeys.class})
public @interface EnabledEvoluumCache {
}
