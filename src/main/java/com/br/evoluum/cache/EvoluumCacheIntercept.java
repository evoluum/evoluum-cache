package com.br.evoluum.cache;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;

@Aspect
@Component
public class EvoluumCacheIntercept {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ExpiratedTime expiratedTime;

    @Autowired
    private CalculareTime calculareTime;

    @Autowired
    private GenerateKeys generateKeys;

    @Around("@annotation(com.br.evoluum.cache.EvoluumCacheable) && execution(* *(..))")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        String[] parameterNames = signature.getParameterNames();
        Object[] args = joinPoint.getArgs();

        //*************
        // GERANDO KEYS

        EvoluumCacheable evoluumCache = method.getAnnotation(EvoluumCacheable.class);

        String[] keys = generateKeys.execute(
                signature.getMethod().getDeclaringClass().getSimpleName(),
                signature.getName(),
                evoluumCache.key(),
                parameterNames,
                args
        );

        String key = keys[0];
        String keyDate = keys[1];

        //**********************************
        CacheObject cacheObject = (CacheObject) cacheService.cacheGet(keyDate);

        if (cacheObject == null) { // cache nao exite
            // consultando o metodo origina
            Object objectReturn = joinPoint.proceed();

            putTime(keyDate, evoluumCache.expires(), evoluumCache.entityKey());
            put(key, objectReturn);

            return objectReturn;
        } else { // cache existe
            boolean expired = expiratedTime.execute(cacheObject.getTimeExpirate(), new Date());
            if (expired) { // o valor está espirado, assim ele vai tentar buscar os dados.
                try {
                    Object objectReturn = joinPoint.proceed();

                    // atualizando os dados no redis com as novas
                    putTime(keyDate, evoluumCache.expires(), evoluumCache.entityKey());
                    put(key, objectReturn);

                    return objectReturn;
                } catch (Exception e) {
                    if (CheckedException.check(e)) {
                        // vamos retornar os dados do cache, mesmo desatualizado, pq o serviço nao está disponivel
                        return cacheService.cacheGet(key);
                    }
                    throw e;
                }
            }
        }

        return cacheService.cacheGet(key);
    }

    private void putTime(String key, CacheExpires cacheExpires, String entityKey) {
        // gerando hora para data de expiração
        String[] sum = calculareTime.sum(cacheExpires, new Date());

        CacheObject cacheObject = CacheObject
                .builder()
                .timeCreated(sum[0])
                .timeExpirate(sum[1])
                .entityKey(entityKey)
                .build();

        cacheService.cachePut(key, cacheObject);
    }

    private void put(String key, Object objectReturn) {
        cacheService.cachePut(key, objectReturn);
    }
}
