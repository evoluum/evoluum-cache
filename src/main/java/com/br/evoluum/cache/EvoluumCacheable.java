package com.br.evoluum.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotação que marca o método para fazer Cache.
 *
 * Existe 2 formas de utilizar a anotação;
 *
 * Exemplo 1
 * ------------------
 * @EvoluumCacheable(name="nomeQualquer", expires=CacheExpires.THIRTY_SECONDS)
 * public Object xx(String cpf)
 *
 * xx(05643089889)
 *
 * Neste exemplo, a clave gerar será "nomeQualquer#xx#05643089889#"
 * Ele vai pegar o valor da propriedade que foi enviada para o método
 * ============================================
 *
 * Exemplo 2
 * ------------------
 * @EvoluumCacheable(name="nomeQualquer", keys="#pessoa.cpf", expires=CacheExpires.THIRTY_SECONDS)
 * public Object xx(Pessoa pessoa)
 *
 * xx(new Pessoa("05643089889"))
 *
 * Neste exemplo, a clave gerar será "nomeQualquer#xx#05643089889#"
 * Ele vai pegar o valor da propriedade dentro d bjeto passado.
 * Na definição da key é preciso montar com #nomePropriedadeMetodo.propriedadeObjeto.
 * ============================================
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EvoluumCacheable {

    String name();
    String[] key() default {};
    CacheExpires expires();

    String entityKey();
}
