package com.br.evoluum.cache;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class ExpiratedTime {

    public boolean execute(String time, Date dateNow) throws ParseException {

        final DateFormat dt = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");

        final Calendar dtStart = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        dtStart.setTime(dt.parse(time));

        final Calendar dtEnd = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        dtEnd.setTime(dateNow);

        return dtEnd.after(dtStart) || dtEnd.equals(dtStart);
    }
}
