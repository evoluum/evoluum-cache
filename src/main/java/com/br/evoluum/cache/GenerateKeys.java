package com.br.evoluum.cache;

public class GenerateKeys {

    private static final String STR = "#";
    private static final String DATE = "_date";

    /**
     * Essa classe gera 2 key.
     * A primeiro é para ser usada para guardar o objeto de retorno
     * A segunda é usada para guardar a data
     * @param methodName
     * @param name
     * @param keys
     * @param parameterNames
     * @param args
     * @return
     */
    public String[] execute(String methodName, String name, String[] keys,
                            String[] parameterNames, Object[] args) {

        StringBuilder keyBuilder = new StringBuilder();
        keyBuilder.append(methodName).append(STR);
        keyBuilder.append(name).append(STR);

        if (keys == null || keys.length <= 0 ) {
            for (int loop = 0; loop < parameterNames.length; loop++) {
                keyBuilder.append(args[loop]).append(STR);
            }
        } else {
            for (String properties : keys) {

                String nameClass = properties;

                if( properties.indexOf("#") >= 0 && properties.indexOf(".") >= 0 ){
                    nameClass = properties.substring(properties.indexOf("#")+1, properties.indexOf("."));
                }

                for (int loop = 0; loop < parameterNames.length; loop++) {
                    if( nameClass.equals(parameterNames[loop])){

                        String nameProperties = properties.substring(properties.indexOf(".")+1, properties.length());

                        Object value = GetFieldsValue.get(args[loop], nameProperties);
                        keyBuilder.append(value).append(STR);
                    }
                }
            }
        }


        String key = keyBuilder.toString();
        String keyDate = keyBuilder.toString().concat(DATE);

        return new String[]{key, keyDate};
    }

}