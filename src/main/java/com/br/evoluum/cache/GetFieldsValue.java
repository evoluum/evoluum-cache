package com.br.evoluum.cache;

import java.lang.reflect.Field;
import java.util.UUID;

public class GetFieldsValue {

    public static Object get(Object object, String key) {

        if (object == null) {
            return null;
        }

        if( object instanceof Enum ||
                object instanceof String  ||
                object instanceof Integer ||
                object instanceof Long ||
                object instanceof UUID){
            return object.toString();
        }

        try {
            Class<?> oClass = Class.forName(String.valueOf(object.getClass().getName()));

            Field[] fields = oClass.getDeclaredFields();
            if (fields == null) {
                return null;
            }

            for (Field field : fields) {
                if (key.equals(field.getName())) {
                    field.setAccessible(true);
                    return field.get(object);
                }
            }

        } catch (ClassNotFoundException | IllegalAccessException e) {
            return null;
        }

        return null;
    }

}
