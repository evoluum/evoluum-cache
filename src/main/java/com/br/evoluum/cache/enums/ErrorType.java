package com.br.evoluum.cache.enums;

public enum ErrorType {

    LOG_ERROR("Erro inesperado: "),
    GENERIC_ERROR("generic_error"),
    METHOD_ARG_NOT_VALID_ERROR("Argumento ou valor(es) inválido(s)."),
    BAD_REQUEST("Um ou mais parâmetros está em formato incorreto."),
    UNEXPECTED_ERROR("Um erro inexperado ocorreu, por favor, verifique os logs."),
    INVALID_DATE_FORMAT("A data informada é inválida."),
    KAFKA_TIMED_OUT("Reply timed out"),
    CUSTOMER_ERP_NOT_FOUND("O cliente não está cadastrado na base de dados."),
    UNAUTHORIZED("O token informado é inválido."),
    INTERNAL_SERVER_ERROR("O serviço do erp Unimed retornou um erro desconhecido."),
    BAD_GATEWAY("O retorno do serviço do erp Unimed não está em formato conhecido."),
    SERVICE_UNAVAILABLE("O serviço do erp Unimed está indisponível no momento, tente novamente mais tarde."),
    GATEWAY_TIMEOUT("O serviço não respondeu."),
    INVALID_CPF("O CPF informado não é valido.");

    private String error;

    ErrorType(String error) {
        this.error = error;
    }

    public String getMessage() {
        return this.error;
    }
}

