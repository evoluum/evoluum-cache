package com.br.evoluum.cache.exceptions;

import com.br.evoluum.cache.enums.ErrorType;

public class BadGatewayException extends ApmException implements ExternalError {

    public BadGatewayException(Exception ex) {
        super(ErrorType.BAD_GATEWAY.getMessage(), ErrorType.BAD_GATEWAY.toString(), ex);
    }
}