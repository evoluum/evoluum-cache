package com.br.evoluum.cache.exceptions;

import com.br.evoluum.cache.enums.ErrorType;

public class GatewayTimeoutException extends ApmException implements ExternalError {

    public GatewayTimeoutException(Exception ex) {
        super(ErrorType.GATEWAY_TIMEOUT.getMessage(), ErrorType.GATEWAY_TIMEOUT.toString(), ex);
    }
}