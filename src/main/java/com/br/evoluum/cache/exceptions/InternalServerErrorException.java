package com.br.evoluum.cache.exceptions;

import com.br.evoluum.cache.enums.ErrorType;

public class InternalServerErrorException extends ApmException  implements ExternalError{

    public InternalServerErrorException(Exception ex) {
        super(ErrorType.INTERNAL_SERVER_ERROR.getMessage(), ErrorType.INTERNAL_SERVER_ERROR.toString(), ex);
    }
}