package com.br.evoluum.cache.exceptions;

import com.br.evoluum.cache.enums.ErrorType;

public class ServiceUnavailableException extends ApmException  implements ExternalError {

    public ServiceUnavailableException(Exception ex) {
        super(ErrorType.SERVICE_UNAVAILABLE.getMessage(), ErrorType.SERVICE_UNAVAILABLE.toString(), ex);
    }
}