package com.br.evoluum.cache;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CalculareTimeTest {

    @Test
    public void test_time() throws ParseException {
        final DateFormat dt = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
        Date parse = dt.parse("2020-10-12T01:01:00");

        CalculareTime calculareTime = new CalculareTime();
        String[] sum = calculareTime.sum(CacheExpires.THIRTY_SECONDS, parse);

        Assert.assertEquals("2020-10-12T01:01:00", sum[0]);
        Assert.assertEquals("2020-10-12T01:01:30", sum[1]);

        sum = calculareTime.sum(CacheExpires.ONE_MINUTE, parse);
        Assert.assertEquals("2020-10-12T01:02:00", sum[1]);

        sum = calculareTime.sum(CacheExpires.ONE_HOUR, parse);
        Assert.assertEquals("2020-10-12T02:01:00", sum[1]);

        sum = calculareTime.sum(CacheExpires.ONE_DAY, parse);
        Assert.assertEquals("2020-10-13T01:01:00", sum[1]);

        sum = calculareTime.sum(CacheExpires.SEVEN_DAYS, parse);
        Assert.assertEquals("2020-10-19T01:01:00", sum[1]);
    }
}
