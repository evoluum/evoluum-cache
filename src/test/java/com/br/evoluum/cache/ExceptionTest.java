package com.br.evoluum.cache;

import com.br.evoluum.cache.exceptions.GatewayTimeoutException;
import org.junit.Test;

public class ExceptionTest {

    @Test
    public void test_exception_capture_name(){

        try{
            throw new GatewayTimeoutException(new Exception());
        }catch (Exception ex){
            ex.toString();
        }

    }
}
