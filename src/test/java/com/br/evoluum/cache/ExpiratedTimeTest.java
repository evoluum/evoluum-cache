package com.br.evoluum.cache;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExpiratedTimeTest {

    @Test
    public void test_time_validade() throws ParseException {

        final DateFormat dt = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
        Date parse = dt.parse("2020-10-12T01:02:00");

        ExpiratedTime expiratedTime = new ExpiratedTime();
        Assert.assertTrue(expiratedTime.execute("2020-10-12T01:01:00",parse));
        Assert.assertTrue(expiratedTime.execute("2020-10-12T01:02:00",parse));
        Assert.assertFalse(expiratedTime.execute("2020-10-12T01:03:00",parse));
        Assert.assertFalse(expiratedTime.execute("2020-10-12T01:02:01",parse));
    }
}
