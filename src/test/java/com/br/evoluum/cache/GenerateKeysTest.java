package com.br.evoluum.cache;

import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class GenerateKeysTest {

    @Test
    public void test_keys(){
        GenerateKeys generateKeys = new GenerateKeys();

        String[] execute = generateKeys.execute("marcelo", "classe",
                null, new String[]{"valor1"},
                new Object[]{"valor1"});

        Assert.assertEquals("marcelo#classe#valor1#", execute[0]);
    }

    @Test
    public void test_keys_object(){
        GenerateKeys generateKeys = new GenerateKeys();

        ObjectTest objectTest = ObjectTest
                .builder()
                .valorString("marcelo2")
                .build();

        String[] execute = generateKeys.execute("marcelo", "classe",
                new String[]{"#objectTest.valorString"}, new String[]{"objectTest"},
                new Object[]{objectTest});

        Assert.assertEquals("marcelo#classe#marcelo2#", execute[0]);
    }

    enum X{
        A;
    }
    @Test
    public void test_keys_object_nultiple(){
        GenerateKeys generateKeys = new GenerateKeys();

        String[] execute = generateKeys.execute("marcelo", "classe",
                null, new String[]{"valor1", "valor2", "valor3"},
                new Object[]{"valor1", UUID.fromString("5344b987-2976-4b9e-b40e-16c24db1f7c6"), X.A});

        Assert.assertEquals("marcelo#classe#valor1#5344b987-2976-4b9e-b40e-16c24db1f7c6#A#", execute[0]);
    }
}
