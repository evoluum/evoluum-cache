package com.br.evoluum.cache;

import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class GetFieldsValueTest {

    @Test
    public void test_get_key_default() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
        ObjectTest objectTest = ObjectTest
                .builder()
                .valorInt(10)
                .valorString("marcelo")
                .build();

        Object value = GetFieldsValue.get(objectTest, "valorInt");
        Assert.assertEquals(10, (int)value);

        value = GetFieldsValue.get(objectTest, "valorString");
        Assert.assertEquals("marcelo", (String)value);

        value = GetFieldsValue.get("marcelo", "valorString");
        Assert.assertEquals("marcelo", (String)value);

        value = GetFieldsValue.get(10, "valorString");
        Assert.assertEquals("10", (String)value);

        value = GetFieldsValue.get(10L, "valorString");
        Assert.assertEquals("10", (String)value);

        value = GetFieldsValue.get(UUID.fromString("752f05b4-80d4-4874-b17c-d28f2b16d204"), "valorString");
        Assert.assertEquals("752f05b4-80d4-4874-b17c-d28f2b16d204", (String)value);
    }

}
