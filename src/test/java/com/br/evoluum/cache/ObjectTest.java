package com.br.evoluum.cache;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ObjectTest {
    private int valorInt;
    private String valorString;
}
